import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import PageContent from '~/components/PageContent';

const RouteWrapper = ({ component: Component, isPrivate, ...rest }) => {
  const { authenticated } = useSelector(state => state.auth);

  if (!authenticated && isPrivate) {
    return <Redirect to="/" />;
  }

  if (authenticated && !isPrivate) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <PageContent {...rest}>
      <Route {...rest}>
        <Component {...rest} />
      </Route>
    </PageContent>
  );
};

RouteWrapper.propTypes = {
  isPrivate: PropTypes.bool,
  component: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func,
    PropTypes.object,
  ]).isRequired,
};

RouteWrapper.defaultProps = {
  isPrivate: false,
};

export default RouteWrapper;
