const routes = [
  {
    title: 'Login',
    component: 'Auth/Login',
    path: '/',
    auth: false,
    exact: true,
  },
  {
    title: 'Cadastrar',
    component: 'Auth/Cadastro',
    path: '/auth/cadastrar',
    auth: false,
    exact: true,
  },
  {
    title: 'Dashboard',
    component: 'Dashboard',
    path: '/dashboard',
    auth: true,
    exact: true,
  },
];

export default routes;
