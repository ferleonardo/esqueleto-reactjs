import React, { useRef } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Form } from '@unform/web';
import * as Yup from 'yup';

import Input from '~/components/Input';

import './style.scss';

import { registrarRequest } from '~/store/modules/auth/actions';

const Cadastro = () => {
  const dispatch = useDispatch();
  const formRef = useRef(null);
  const handleSubmit = async dados => {
    formRef.current.setErrors({});
    try {
      const schema = Yup.object().shape({
        email: Yup.string()
          .email('Por favor insira um email válido.')
          .required('O email é obrigatório.'),
        senha: Yup.string()
          .required('A senha é obrigatória.')
          .min(8, 'No mínimo 8 caracteres.'),
      });

      await schema.validate(dados, {
        abortEarly: false,
      });

      dispatch(registrarRequest(dados));
    } catch (err) {
      const validationErrors = {};
      if (err instanceof Yup.ValidationError) {
        err.inner.forEach(error => {
          validationErrors[error.path] = error.message;
        });
        formRef.current.setErrors(validationErrors);
      }
    }
  };
  return (
    <>
      <h2>Cadastrar</h2>
      <Form ref={formRef} onSubmit={handleSubmit}>
        <Input
          name="email"
          type="email"
          label="Email"
          placeholder="Digite seu email"
        />
        <Input
          name="senha"
          type="password"
          label="Senha"
          placeholder="Digite sua senha"
        />
        <button type="submit">Criar</button>
      </Form>
      <Link to="/">Já tenho uma conta</Link>
    </>
  );
};

export default Cadastro;
