import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { Form } from '@unform/web';

import './style.scss';

import Input from '~/components/Input';

import { loginRequest } from '~/store/modules/auth/actions';

const Login = () => {
  const dispatch = useDispatch();

  function handleSubmit({ email, senha }) {
    dispatch(loginRequest(email, senha));
  }

  return (
    <>
      <h2>Login</h2>
      <Form onSubmit={handleSubmit}>
        <Input name="email" type="email" placeholder="Email" />
        <Input name="senha" type="password" placeholder="Senha" />
        <button type="submit">Entrar</button>
      </Form>
      <Link to="/auth/cadastrar">Criar conta</Link>
    </>
  );
};

export default Login;
