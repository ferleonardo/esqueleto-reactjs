import React from 'react';
import { useDispatch } from 'react-redux';

import { logoutRequest } from '~/store/modules/auth/actions';

export default function Dashboard() {
  const dispatch = useDispatch();

  const handleLogout = () => dispatch(logoutRequest());

  return (
    <>
      <h2>Dashboard</h2>
      <button type="button" onClick={handleLogout}>
        Sair
      </button>
    </>
  );
}
