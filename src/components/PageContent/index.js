import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const PageContent = ({ children }) => (
  <div className="page-content">{children}</div>
);

PageContent.propTypes = {
  children: PropTypes.element,
};

export default PageContent;
