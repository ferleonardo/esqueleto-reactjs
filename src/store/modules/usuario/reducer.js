import produce from 'immer';

import { AuthTypes } from '../auth/actions';

export const INITIAL_STATE = {
  dados: null,
};

export default function usuario(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case AuthTypes.LOGIN_SUCCESS: {
        draft.dados = action.payload.usuario;
        break;
      }
      case AuthTypes.LOGOUT_REQUEST: {
        draft.dados = null;
        break;
      }
      default:
    }
  });
}
