import { all, call, takeLatest, put } from 'redux-saga/effects';

import api from '~/services/api';
import history from '~/services/history';

import { loginSuccess, AuthTypes } from './actions';

export function* login({ payload }) {
  const { email, senha } = payload;
  const response = yield call(api, 'post', 'auth', {
    email,
    senha,
  });

  const { data, status } = response;

  switch (status) {
    case 200:
      // eslint-disable-next-line
      const { token, usuario, expira_em } = data.dados;
      yield put(loginSuccess(token, usuario, expira_em));
      history.push('/dashboard');
      break;

    default:
      console.log(data);
  }
}

export function* registrar({ payload }) {
  const { email, senha } = payload;
  const response = yield call(api, 'post', 'auth/cadastrar', {
    email,
    senha,
  });

  const { data, status } = response;

  if (status === 200) {
    console.log('Cadastro realizado com sucesso', data);
    history.push('/');
  } else {
    console.log(data);
  }
}

export function* logout() {
  yield call(api, 'get', 'auth/sair');
  history.push('/');
}

export default all([
  takeLatest(AuthTypes.LOGIN_REQUEST, login),
  takeLatest(AuthTypes.REGISTRAR_REQUEST, registrar),
  takeLatest(AuthTypes.LOGOUT_REQUEST, logout),
]);
