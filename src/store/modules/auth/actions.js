export const AuthTypes = {
  LOGIN_REQUEST: '@auth/LOGIN_REQUEST',
  LOGIN_SUCCESS: '@auth/LOGIN_SUCCESS',
  LOGOUT_REQUEST: '@auth/LOGOUT_REQUEST',
  REGISTRAR_REQUEST: '@auth/REGISTRAR_REQUEST',
};

export function loginRequest(email, senha) {
  return {
    type: AuthTypes.LOGIN_REQUEST,
    payload: { email, senha },
  };
}

export function loginSuccess(token, usuario, expira_em) {
  return {
    type: AuthTypes.LOGIN_SUCCESS,
    payload: { token, usuario, expira_em },
  };
}

export function logoutRequest() {
  return {
    type: AuthTypes.LOGOUT_REQUEST,
  };
}

export function registrarRequest({ email, senha }) {
  return {
    type: AuthTypes.REGISTRAR_REQUEST,
    payload: { email, senha },
  };
}
