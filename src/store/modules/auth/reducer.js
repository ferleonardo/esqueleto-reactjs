import produce from 'immer';

import { AuthTypes } from './actions';

export const INITIAL_STATE = {
  token: null,
  authenticated: false,
  expira_em: null,
};

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case AuthTypes.LOGIN_SUCCESS: {
        draft.token = action.payload.token;
        draft.authenticated = true;
        draft.expira_em = action.payload.expira_em;
        break;
      }
      case AuthTypes.LOGOUT_REQUEST: {
        draft.token = null;
        draft.authenticated = false;
        break;
      }
      default:
    }
  });
}
