import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import {
  render,
  cleanup,
  fireEvent,
  waitForElement,
} from '@testing-library/react';

import { http } from '~/services/api';
import App from '~/App';

const apiMock = new MockAdapter(http);

describe('Funcionalidades de Login', () => {
  afterEach(() => {
    cleanup();
  });

  test('Renderiza página inicial com formulário de login', async () => {
    const { getByPlaceholderText, getByText } = render(<App />);
    await waitForElement(() => [
      getByPlaceholderText('Email'),
      getByPlaceholderText('Senha'),
      getByText('Entrar'),
    ]);
  });

  test('Preenche formulário e realiza login com sucesso', async () => {
    const { getByPlaceholderText, getByText } = render(<App />);
    const [inputEmail, inputSenha, submit] = await waitForElement(() => [
      getByPlaceholderText('Email'),
      getByPlaceholderText('Senha'),
      getByText('Entrar'),
    ]);

    fireEvent.input(inputEmail, {
      target: { value: 'ferleonardo@gmail.com' },
    });
    fireEvent.input(inputSenha, {
      target: { value: 12345678 },
    });
    fireEvent.click(submit);

    apiMock.onPost('auth').reply(200, {
      mensagem: 'Login efetuado com sucesso.',
      dados: {
        usuario: {
          id: 1,
          nome: 'Leonardo Fernandes',
          email: 'ferleonardo@gmail.com',
        },
        token: 'hadouken.shoryuken.signature',
        expira_em: '1',
      },
    });

    await waitForElement(() => [getByText('Dashboard'), getByText('Sair')]);
  });

  test('Clica no botão sair e efetua logout com sucesso', async () => {
    const { getByText } = render(<App />);
    const sair = await waitForElement(() => getByText('Sair'));

    fireEvent.click(sair);

    await waitForElement(() => [getByText('Login'), getByText('Entrar')]);
  });
});
