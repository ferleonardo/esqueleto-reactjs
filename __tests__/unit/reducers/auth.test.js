import reducer, { INITIAL_STATE } from '~/store/modules/auth/reducer';
import * as Actions from '~/store/modules/auth/actions';

describe('Auth reducers', () => {
  const token = 'hadouken.shoriuken.signature';
  const usuario = {
    id: 1,
    email: 'ferleonardo@gmail.com',
    nome: 'Leonardo Fernandes',
  };
  const expira_em = 10;

  it(Actions.AuthTypes.LOGIN_SUCCESS, () => {
    const state = reducer(
      undefined,
      Actions.loginSuccess(token, usuario, expira_em)
    );

    expect(state).toStrictEqual({ token, authenticated: true, expira_em });
  });

  it(Actions.AuthTypes.LOGOUT_REQUEST, () => {
    const state = reducer(INITIAL_STATE, Actions.logoutRequest());
    expect(state.authenticated).toBeFalsy();
    expect(state.token).toBeNull();
  });
});
